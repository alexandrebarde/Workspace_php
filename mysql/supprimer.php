<?php

if(!($_SERVER['HTTP_REFERER'] == 'http://localhost/Workspace_php/mysql/recherche.php' . '?id=' . $_GET['id']))
{
  //header("Location: index.html");
  echo $_SERVER['HTTP_REFERER'];
}
else
{

}

  if(isset($_GET['submit']))
  {
    switch ($_GET['submit'])
    {
      case "Supprimer":
        try
        {
          $pdo = new PDO("mysql:host=localhost;dbname=carnetdadresses", "root", "");
          $pdo->exec("SET CHARACTER SET utf8");
        }
        catch(Exception $e)
        {
          die('Erreur : ' . $e->getMessage());
          exit();
        }
        $id = $_GET['id'];
        $req = $pdo->prepare('DELETE FROM carnet WHERE id = :id');
        $req->execute(array(':id' => $id));
        header("location: recherche.php");
        break;
      case "Annuler":
        header("location: recherche.php");
        break;
    }
  }
  if(isset($_GET['id']))
  {
    try
    {
      $pdo = new PDO("mysql:host=localhost;dbname=carnetdadresses", "root", "");
      $pdo->exec("SET CHARACTER SET utf8");
    }
    catch(Exception $e)
    {
      die('Erreur : ' . $e->getMessage());
      exit();
    }

      $id = $_GET['id'];
      $req = $pdo->prepare('SELECT * FROM carnet WHERE id = :id');
      $req->execute(array(':id' => $id));
      $nombre = $req->rowCount();
      if($nombre == 0)
      {
        echo "Une erreur est survenue.";
        exit();
      }
      else
      {
        while($data = $req->fetch())
        {
          $nom = $data['nom'];
          $prenom = $data['prenom'];
          $adresse = $data['adresse'];
          $ville = $data['ville'];
          $code_postal = $data['code_postal'];
          $telephone = $data['telephone'];
        }
        $req->closeCursor();
      }
    
  }
?>


<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Suppression d'un contact</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
  </head>
  <body>
    <form action="supprimer.php" method="get">
      <fieldset>
        <legend>Suppression d'un contact</legend>
        <p>Supprimer <?php echo ucfirst($nom) . " " . ucfirst($prenom); ?></p>
        <input type="hidden" name="id" value="<?php echo $id; ?>">
        <input type="submit" name="submit" value="Supprimer">
        <input type="submit" name="submit" value="Annuler">
      </fieldset>
    </form>
  </body>
</html>
