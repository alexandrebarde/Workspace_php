<?php
  if(isset($_POST['motCles']))
  {
    try
    {
      $pdo = new PDO("mysql:host=localhost;dbname=carnetdadresses", "root", "");
      $pdo->exec("SET CHARACTER SET utf8");
    }
    catch(Exception $e)
    {
      die('Erreur : ' . $e->getMessage());
      exit();
    }
    if(!($_SERVER['HTTP_REFERER'] == "http://localhost/Workspace_php/mysql/recherche.php"))
    {
      echo "Une erreur est survenue.";
      exit();
    }
    else
    {
      $motCles = strtolower($_POST['motCles']);

      $motClesTab = explode(" ", $motCles);

      $requete = 'SELECT * FROM carnet WHERE ';

      for($i = 0; $i < count($motClesTab); $i++)
      {
        $requete .= "(nom LIKE \"%$motClesTab[$i]%\" OR prenom LIKE \"%$motClesTab[$i]%\" OR adresse LIKE \"%$motClesTab[$i]%\" OR code_postal LIKE \"%$motClesTab[$i]%\" OR ville LIKE \"%$motClesTab[$i]%\" OR telephone LIKE \"%$motClesTab[$i]%\")";
        if($i < count($motClesTab) - 1)
        {
          $requete .= " AND ";
        }
      }

      $req = $pdo->prepare($requete);
      $req->execute();
      $nombre = $req->rowCount();
    }
  }
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Recherche de contacts</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <form action="#" method="post">
      <label for="motCles">Mots-clés (séparé d'un espace)</label>
      <br />
      <input type="text" name="motCles">
      <br />
      <input type="submit" value="Rechercher">
    </form>
    <br />
    <?php
      if(isset($_POST['motCles']))
      {
        if($nombre == 0)
        {
          echo "Aucun contact n'a été trouvé dans la base de données.";
          exit();
        }
        echo "<table><tr><th>Nom</th><th>Prénom</th><th>Adresse</th><th>Code postal</th><th>Ville</th><th>Téléphone</th><th>Modifier</th><th>Supprimer</th></tr>";
        while($data = $req->fetch())
        {
          echo "<tr>" . "<td>" . $data['nom'] . "</td><td>" . $data['prenom'] . "</td><td>" . $data['adresse'] . "</td><td>" . $data['code_postal'] . "</td><td>" . $data['ville'] . "</td><td>" . $data['telephone'] . "</td>" . "<td><a href='modifier.php?id=" . $data['id'] . "'>Modifier</td>" . "<td><a href='supprimer.php?id=" . $data['id'] . "'>Supprimer</td></tr>";
        }
        echo "</table>";
        $req->closeCursor();
      }
    ?>
  </body>
</html>
