<?php
  if(isset($_GET['id']))
  {
    try
    {
      $pdo = new PDO("mysql:host=localhost;dbname=carnetdadresses", "root", "");
      $pdo->exec("SET CHARACTER SET utf8");
    }
    catch(Exception $e)
    {
      die('Erreur : ' . $e->getMessage());
      exit();
    }
    if(!($_SERVER['HTTP_REFERER'] == "http://localhost/Workspace_php/mysql/recherche.php"))
    {
      header("Location: index.html");
      //echo "Une erreur est survenue.";
      exit();
    }
    else
    {
      $id = $_GET['id'];
      $req = $pdo->prepare('SELECT * FROM carnet WHERE id = :id');
      $req->execute(array(':id' => $id));
      $nombre = $req->rowCount();
      if($nombre == 0)
      {
        echo "Une erreur est survenue.";
        exit();
      }
      else
      {
        while($data = $req->fetch())
        {
          $nom = $data['nom'];
          $prenom = $data['prenom'];
          $adresse = $data['adresse'];
          $ville = $data['ville'];
          $code_postal = $data['code_postal'];
          $telephone = $data['telephone'];
        }
        $req->closeCursor();
      }
    }
  }
  else
  {
    header("Location: index.html");
  }
?>


<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Modification d'un contact</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
  </head>
  <body>
    <form action="modification.php" method="post">
      <fieldset>
        <legend>Modification d'un contact</legend>
        <label for="nom">Nom</label>
        <input type="text" name="prenom" value="<?php echo $nom; ?>"/>
        <br />
        <label for="prenom">Prénom</label>
        <input type="text" name="nom" value="<?php echo $prenom; ?>"/>
        <br />
        <label for="adresse">Adresse</label>
        <input type="text" name="adresse" value="<?php echo $adresse; ?>"/>
        <br />
        <label for="cp">Code postal</label>
        <input type="text" name="cp" value="<?php echo $code_postal; ?>"/>
        <br />
        <label for="ville">Ville</label>
        <input type="text" name="ville" value="<?php echo $ville; ?>"/>
        <br />
        <label for="tel">Téléphone</label>
        <input type="text" name="tel" value="<?php echo $telephone; ?>"/>
        <br />
        <input type="hidden" name="id" value="<?php echo $id; ?>"/>
        <input type="submit" value="Modifier">
      </fieldset>
    </form>
  </body>
</html>
