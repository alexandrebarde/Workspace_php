-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  ven. 03 nov. 2017 à 17:34
-- Version du serveur :  10.1.26-MariaDB
-- Version de PHP :  7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `carnetdadresses`
--

-- --------------------------------------------------------

--
-- Structure de la table `carnet`
--

CREATE TABLE `carnet` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `adresse` varchar(255) NOT NULL,
  `code_postal` int(11) NOT NULL,
  `ville` varchar(255) NOT NULL,
  `telephone` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `carnet`
--

INSERT INTO `carnet` (`id`, `nom`, `prenom`, `adresse`, `code_postal`, `ville`, `telephone`) VALUES
(7, 'alexandre', 'brd', 'aze', 123123, 'azek', 123123),
(9, 'camille ', 'toups', '917 jett lane', 91505, 'burbank', 2147483647),
(10, 'damiane', 'grenier', '12, boulevard d\'alsace', 92170, 'vanves', 166584506),
(11, 'aceline ', 'lapresse', '86, place charles de gaulle', 59491, 'villeneuve-d\'ascq ', 359554548),
(12, 'latimer ', 'boucher', '18, place de la gare', 31770, 'colomiers', 502865770),
(13, 'pensee ', 'cinqmars', '42, faubourg saint honoré', 75019, 'paris', 188309843),
(14, 'tyson ', 'covillon', '51, rue des lacs', 62110, 'hônin-beaumont ', 375135102),
(15, 'delmare ', 'courtemanche', '92, place charles de gaulle', 33140, 'villenave-d\'ornon ', 573497549),
(16, 'alphonsine ', 'chartré', '87, rue de penthièvre', 95000, 'pontoise', 372091163),
(17, 'medoro ', 'tétrault', '47, avenue des pr\'es', 95160, 'montmorency', 126313069),
(18, 'alexandre ', 'ratté', '52, rue lenotre', 78120, 'rambouillet', 118847824);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `carnet`
--
ALTER TABLE `carnet`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `carnet`
--
ALTER TABLE `carnet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
