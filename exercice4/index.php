<?php

  date_default_timezone_set('UTC');

  echo date("d/m/Y ");
  echo "il est ";
  echo date("G:i:s");

  echo "\n<br />";

  if(estBissextile(2017) == true)
  {
    echo "Cette année est bissextile.";
  }
  else
  {
    echo "Cette année n'est pas bissextile.";
  }

  function estBissextile($annee)
  {
    if($annee % 4 != 0)
    {
      return false;
    }
    elseif(($annee % 4 == 0) && ($annee % 100 == 0) || ($annee % 400 != 0))
    {
      return true;
    }
    else
    {
      return false;
    }
  }

?>
