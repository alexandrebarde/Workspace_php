<?php

  if(isset($_GET['nombre1']))
  {
    $nombre1 = intval($_GET['nombre1']);
  }
  else
  {
    $nombre1 = 0;
  }

  if(isset($_GET['nombre2']))
  {
    $nombre2 = intval($_GET['nombre2']);
  }
  else
  {
    $nombre2 = 0;
  }

  if(isset($_GET['submit']))
  {
    switch ($_GET['submit'])
    {
      case "additionner":
        $tmp = $nombre1 + $nombre2;
        $resultat = '<input type="text" name="resultat" value="' . $tmp . '"/>';
        break;
      case "soustraire":
        $tmp = $nombre1 - $nombre2;
        $resultat = '<input type="text" name="resultat" value="' . $tmp . '"/>';
        break;
      case "multiplier":
        $tmp = $nombre1 * $nombre2;
        $resultat = '<input type="text" name="resultat" value="' . $tmp . '"/>';
        break;
      case "diviser":
        $tmp = $nombre1 / $nombre2;
        $resultat = '<input type="text" name="resultat" value="' . $tmp . '"/>';
        break;
    }
  }
  else
  {
    $resultat = '<input type="text" name="nombre1" value=""/>';
  }



  echo '<form method="get" action="#">';
  echo '<p>';
  echo '<label for="nombre1">Nombre 1</label>';
  echo '<input type="text" name="nombre1" value="' . $nombre1 . '"/>';
  echo '</p>';
  echo '<p>';
  echo '<label for="nombre2">Nombre 2</label>';
  echo '<input type="text" name="nombre2" value="' . $nombre2 . '"/>';
  echo '</p>';
  echo '<p>';
  echo '<label for="resultat">Résultat</label>';
  echo $resultat;
  echo '</p>';
  echo "Opérateur";
  echo '<input type="submit" value="additionner" name="submit" />';
  echo '<input type="submit" value="soustraire" name="submit" />';
  echo '<input type="submit" value="multiplier" name="submit" />';
  echo '<input type="submit" value="diviser" name="submit" />';
  echo '</form>';

?>
