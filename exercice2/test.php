<?php

  $age = 20;
  echo "Version  1 : J'ai $age ans.";
  echo "<br />"; //Retour à la ligne
  echo "Version 2 : J'ai " . $age . " ans.";

  $chaine2 = "J'aime le veau !";
  $chaine1 = $chaine2;

  echo "<br /> <br />"; //Retour à la ligne

  if($chaine1 == $chaine2)
  {
    echo "Version 1 : Les 2 variables sont identiques !";
  }
  else
  {
    echo "Version 1 : Les 2 variables sont différentes !";
  }

  echo "<br />"; //Retour à la ligne

  if(strcmp($chaine1, $chaine2) == 0)
  {
    echo "Version 2 : Les 2 variables sont identiques !";
  }
  else
  {
    echo "Version 2 : Les 2 variables sont différentes !";
  }

  echo "<br /> <br />"; //Retour à la ligne

  if(strcmp("bonjour", "Bonjour") == 0)
  {
    echo "Bonjour et bonjour sont identiques !";
  }
  else
  {
    echo "Bonjour et bonjour ne sont pas identiques !";
  }

?>
